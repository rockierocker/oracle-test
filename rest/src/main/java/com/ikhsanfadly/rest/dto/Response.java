package com.ikhsanfadly.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {

    private SampleServiceRs sampleservicers;

    @Getter
    @Setter
    public static class SampleServiceRs{

        private String errorCode;

        private String errorMsg;

        private String trxId;


    }

}
