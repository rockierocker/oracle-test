package com.ikhsanfadly.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Request {

    private SampleServiceRq sampleservicerq;

    @Getter
    @Setter
    public static class SampleServiceRq{

        private String serviceId;

        private String orderType;

        private String type;

        private String trxId;

    }

}
