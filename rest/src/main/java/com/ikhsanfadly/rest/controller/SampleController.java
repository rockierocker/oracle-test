package com.ikhsanfadly.rest.controller;

import com.ikhsanfadly.rest.dto.Request;
import com.ikhsanfadly.rest.dto.Response;
import com.ikhsanfadly.rest.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/sample-service")
@RestController
public class SampleController {

    @Autowired
    SampleService sampleService;

    /*
        windows curl
        curl --location "http://localhost:8081/external/services/rest/sample-service" ^
        --header "Accept: application/json" ^
        --header "Content-Type: application/json" ^
        --d "{ \"sampleservicerq\": { \"service_id\": \"20200421201455122\", \"order_type\": \"PRO\", \"type\": \"20221120233514\", \"trx_id\": \"c6714ec0-b379-11e9-889b-7f7167f4f72d\" } }"
     */
    @PostMapping(value = {"","/"})
    public Response sample(@RequestBody Request request){
        return sampleService.sample(request);
    }

}
