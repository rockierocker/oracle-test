package com.ikhsanfadly.rest.service;

import com.ikhsanfadly.rest.dto.Request;
import com.ikhsanfadly.rest.dto.Response;
import org.springframework.stereotype.Service;


@Service
public class SampleServiceImpl implements SampleService{

    @Override
    public Response sample(Request request) {
        Response response = new Response();
        Response.SampleServiceRs sampleServiceRs = new Response.SampleServiceRs();
        sampleServiceRs.setTrxId(request.getSampleservicerq().getTrxId());
        sampleServiceRs.setErrorCode("0000");
        sampleServiceRs.setErrorMsg("Success");
        response.setSampleservicers(sampleServiceRs);
        return response;
    }
}
