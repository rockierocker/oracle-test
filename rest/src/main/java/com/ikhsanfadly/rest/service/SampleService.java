package com.ikhsanfadly.rest.service;

import com.ikhsanfadly.rest.dto.Request;
import com.ikhsanfadly.rest.dto.Response;

public interface SampleService {
    Response sample(Request request);

}
