# oracle-test

## Required
- [x] Maven
- [X] Java 8

## How to run
- [x] cd to directory rest and run maven 'mvn spring-boot:run'
- [x] cd to directory ws and run maven 'mvn spring-boot:run'

## How to test
- [x] ws : open wsdl contract http://localhost:8080/external/services/ws/sampleServiceWsdl.wsdl into SOAP Ui or another tool
- [x] rest : run curl

```
windows curl
curl --location "http://localhost:8081/external/services/rest/sample-service" ^
--header "Accept: application/json" ^
--header "Content-Type: application/json" ^
--d "{ \"sampleservicerq\": { \"service_id\": \"20200421201455122\", \"order_type\": \"PRO\", \"type\": \"20221120233514\", \"trx_id\": \"c6714ec0-b379-11e9-889b-7f7167f4f72d\" } }"

```