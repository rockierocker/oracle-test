package com.ikhsanfadly.ws.endpoint;

import com.ikhsanfadly.ws.service.SampleService;
import com.oracle.SampleRequest;
import com.oracle.SampleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class SampleServiceEndpoint {
    private static final String NAMESPACE_URI = "http://www.oracle.com";

    @Autowired
    SampleService sampleService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SampleRequest")
    @ResponsePayload
    public SampleResponse sample(@RequestPayload SampleRequest request) {
        return sampleService.sample(request);
    }
}