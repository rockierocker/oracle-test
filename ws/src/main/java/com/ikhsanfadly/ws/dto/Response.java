package com.ikhsanfadly.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {

    private SampleServiceRs sampleservicers;

    @Getter
    @Setter
    public static class SampleServiceRs{

        @JsonProperty("error_code")
        private String errorCode;

        @JsonProperty("error_msg")
        private String errorMsg;

        @JsonProperty("trx_id")
        private String trxId;


    }

}
