package com.ikhsanfadly.ws.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Request {

    private SampleServiceRq sampleservicerq;

    @Getter
    @Setter
    public static class SampleServiceRq{

        @JsonProperty("service_id")
        private String serviceId;

        @JsonProperty("order_type")
        private String orderType;

        @JsonProperty("type")
        private String type;

        @JsonProperty("trx_id")
        private String trxId;

    }

}
