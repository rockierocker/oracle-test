package com.ikhsanfadly.ws.service;

import com.ikhsanfadly.ws.dto.Request;
import com.ikhsanfadly.ws.dto.Response;
import com.oracle.SampleRequest;
import com.oracle.SampleResponse;
import com.oracle.SampleServiceRq;
import com.oracle.SampleServiceRs;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SampleServiceImpl implements SampleService{

    @Autowired
    RestTemplate restTemplate;

    @Value("${rest.baseUrl}")
    String baseUrl;

    @Override
    public SampleResponse sample(SampleRequest request) {
        Request newRequest = new Request();
        Request.SampleServiceRq sampleServiceRq = new Request.SampleServiceRq();
        sampleServiceRq.setServiceId(request.getSampleservicerq().getServiceId());
        sampleServiceRq.setType(request.getSampleservicerq().getType());
        sampleServiceRq.setOrderType(request.getSampleservicerq().getOrderType());
        sampleServiceRq.setTrxId(request.getSampleservicerq().getTrxId());
        newRequest.setSampleservicerq(sampleServiceRq);

        Response response = restTemplate.postForObject(baseUrl+"/sample-service",newRequest,Response.class);

        SampleResponse sampleResponse = new SampleResponse();
        SampleServiceRs sampleServiceRs = new SampleServiceRs();
        sampleServiceRs.setErrorCode(response.getSampleservicers().getErrorCode());
        sampleServiceRs.setTrxId(sampleServiceRq.getTrxId());
        sampleServiceRs.setErrorMsg(sampleServiceRs.getErrorMsg());
        sampleResponse.setSampleservicerq(sampleServiceRs);
        return sampleResponse;
    }

}
