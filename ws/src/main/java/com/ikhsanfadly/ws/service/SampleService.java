package com.ikhsanfadly.ws.service;


import com.oracle.SampleRequest;
import com.oracle.SampleResponse;

public interface SampleService {

    SampleResponse sample(SampleRequest request);

}
