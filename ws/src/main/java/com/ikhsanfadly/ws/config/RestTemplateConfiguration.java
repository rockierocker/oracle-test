package com.ikhsanfadly.ws.config;

import com.ikhsanfadly.ws.exception.RestTemplateExceptionHandler;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfiguration {

    @Bean
    RestTemplate restTemplate(){
        RestTemplate restTemplate =  new RestTemplateBuilder()
                .defaultHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader("Accept", MediaType.APPLICATION_JSON_VALUE).build();
        restTemplate.setErrorHandler(new RestTemplateExceptionHandler());
        return restTemplate;
    }
}

